throttle_illum_hw="24/1"


function wait(seconds)
  local start = os.time()
  print("Wait for "..tostring(seconds))
  repeat until os.time() > start + seconds
  print("done")
end

--[[
4250  ixeg/733/le_devices/le_ext_main_ann
4251  ixeg/733/le_devices/le_flap_full_left_ann
4252  ixeg/733/le_devices/le_flap_full_right_ann
4253  ixeg/733/le_devices/le_flap_transit_left_ann
4254  ixeg/733/le_devices/le_flap_transit_right_ann
4255  ixeg/733/le_devices/le_slat_ext_left_ann
4256  ixeg/733/le_devices/le_slat_ext_right_ann
4257  ixeg/733/le_devices/le_slat_full_left_ann
4258  ixeg/733/le_devices/le_slat_full_right_ann
4259  ixeg/733/le_devices/le_slat_transit_left_ann
4260  ixeg/733/le_devices/le_slat_transit_right_ann
4261  ixeg/733/le_devices/le_transit_main_ann
]]

-- Led flaps
le1="20/12"
le2="20/13"
le3="20/30"
le4="20/24"
lee="23/1"  -- LE Flap Ext
lef="23/2"  -- LE Flap Full Ext

let1="20/9"
let2="20/11"
let4="20/29"
let3="20/26"
let="23/0"  -- LE Flap Transit

-- slats
les1f="20/4"
les2f="20/5"
les3f="20/10"
les4f="20/25"
les5f="20/20"
les6f="20/18"
les1e="20/1"
les2e="20/6"
les3e="20/7"
les4e="20/22"
les5e="20/21"
les6e="20/16"
les1t="20/3"
les2t="20/2"
les3t="20/8"
les4t="20/23"
les5t="20/19"
les6t="20/17"

function le_ext_main_ann(v)
  if v ~=0 then v=1 end
  fsim.chan_set(lee, v)
end
function le_flap_full_left_ann(v)
  if v ~= 0 then v=1 end
  fsim.chan_set(le1, v)
  fsim.chan_set(le2, v)
  fsim.chan_set(lef, v)
end

function le_flap_full_right_ann(v)
  if v ~= 0 then v=1 end
  fsim.chan_set(le3, v)
  fsim.chan_set(le4, v)
  fsim.chan_set(lef, v)
end

function le_flap_transit_left_ann(v)
  if v ~= 0 then v=1 end
  fsim.chan_set(let1, v)
  fsim.chan_set(let2, v)
end

function le_flap_transit_right_ann(v)
  if v ~= 0 then v=1 end
  fsim.chan_set(let3, v)
  fsim.chan_set(let4, v)
end

function le_slat_ext_left_ann(v)
  if v ~= 0 then v=1 end
  fsim.chan_set(les1e, v)
  fsim.chan_set(les2e, v)
  fsim.chan_set(les3e, v)
end
function le_slat_ext_right_ann(v)
  if v ~= 0 then v=1 end
  fsim.chan_set(les4e, v)
  fsim.chan_set(les5e, v)
  fsim.chan_set(les6e, v)
end

function le_slat_full_left_ann(v)
  if v ~= 0 then v=1 end
  fsim.chan_set(les1f, v)
  fsim.chan_set(les2f, v)
  fsim.chan_set(les3f, v)
end

function le_slat_full_right_ann(v)
  if v ~= 0 then v=1 end
  fsim.chan_set(les4f, v)
  fsim.chan_set(les5f, v)
  fsim.chan_set(les6f, v)
end

function le_slat_transit_left_ann(v)
  if v ~= 0 then v=1 end
  fsim.chan_set(les1t, v)
  fsim.chan_set(les2t, v)
  fsim.chan_set(les3t, v)
end

function le_slat_transit_right_ann(v)
  if v ~= 0 then v=1 end
  fsim.chan_set(les4t, v)
  fsim.chan_set(les5t, v)
  fsim.chan_set(les6t, v)
end

function le_transit_main_ann(v)
  if v ~= 0 then v=1 end
  fsim.chan_set(let, v)
end

fsim.iocp_evt(4250 ,"le_ext_main_ann")
fsim.iocp_evt(4251 ,"le_flap_full_left_ann")
fsim.iocp_evt(4252 ,"le_flap_full_right_ann")
fsim.iocp_evt(4253 ,"le_flap_transit_left_ann")
fsim.iocp_evt(4254 ,"le_flap_transit_right_ann")
fsim.iocp_evt(4255 ,"le_slat_ext_left_ann")
fsim.iocp_evt(4256 ,"le_slat_ext_right_ann")
fsim.iocp_evt(4257 ,"le_slat_full_left_ann")
fsim.iocp_evt(4258 ,"le_slat_full_right_ann")
fsim.iocp_evt(4259 ,"le_slat_transit_left_ann")
fsim.iocp_evt(4260 ,"le_slat_transit_right_ann")
fsim.iocp_evt(4261 ,"le_transit_main_ann")


-- Landing gear lights--
--[[

4200  ixeg/733/gear/gear_left_red_ann
4201  ixeg/733/gear/gear_right_red_ann
4202  ixeg/733/gear/gear_nose_red_ann
4203  ixeg/733/gear/gear_left_green_ann
4204  ixeg/733/gear/gear_right_green_ann
4205  ixeg/733/gear/gear_nose_green_ann
1460 sim/flightmodel/failures/onground_any
]]
local in_ground

function lock_handle(v)
  if v>0 then
    fsim.chan_set("21/8", 0)
    in_ground = 0
  else
    fsim.chan_set("21/8", 1)
    in_ground = 1
  end
end

function gear_light_red_left(v)
  local chan = "21/2"
  if v>0 then
    fsim.chan_set(chan, 1)
  else
    fsim.chan_set(chan, 0)
  end
end
function gear_light_green_left(v)
  local chan = "21/3"
  if v>0 then fsim.chan_set(chan, 1) else fsim.chan_set(chan, 0) end
end
function gear_light_red_right(v)
  local chan = "21/5"
  if v>0 then fsim.chan_set(chan, 1) else fsim.chan_set(chan, 0) end
end
function gear_light_green_right(v)
  local chan = "21/4"
  if v>0 then fsim.chan_set(chan, 1) else fsim.chan_set(chan, 0) end
end
function gear_light_red_nose(v)
  local chan = "21/0"
  if v>0 then fsim.chan_set(chan, 1) else fsim.chan_set(chan, 0) end
end
function gear_light_green_nose(v)
  local chan = "21/1"
  if v>0 then fsim.chan_set(chan, 1) else fsim.chan_set(chan, 0) end
end

fsim.iocp_evt("4200", "gear_light_red_left")
fsim.iocp_evt(4201, "gear_light_red_right")
fsim.iocp_evt(4202, "gear_light_red_nose")
fsim.iocp_evt(4203, "gear_light_green_left")
fsim.iocp_evt(4204, "gear_light_green_right")
fsim.iocp_evt(4205, "gear_light_green_nose")
fsim.iocp_evt(1460, "lock_handle")


-- 4206 ixeg/733/gear/gear_handle_act
local handle_up, handle_down, fsim_handle_value

function process_gear_handle()
  local val = 5000

  if handle_up == 1 then val=0 end
  if handle_down == 1 then val = 10000 end
  if handle_up == 0 and handle_down == 0 then
    val = 5000
  end
  --print("In process fsim="..tostring(fsim_handle_value).." gear handle_up="..tostring(handle_up).." handle_down="..tostring(handle_down).." write="..tostring(val))
  if val ~= fsim_handle_value then
    fsim.iocp_write(4206, val)
      fsim_handle_value = val
  end
end

function gear_up(v)
  if v == 0 then handle_up = 1 else handle_up = 0 end
  process_gear_handle()
end

function gear_down(v)
  if v == 0 then handle_down = 1 else handle_down = 0 end
  process_gear_handle()
end

function fsim_gear_handle_change(v)
  print("Fsim CB changed now="..tostring(v))
  fsim_handle_value = v
end

fsim.chan_evt("21/24", "gear_down")
fsim.chan_evt("21/25", "gear_up")
fsim.iocp_evt("4206", "fsim_gear_handle_change")

-- roll --
yoke_roll_ratio_xplane = 2
yoke_heading_ratio_xplane = 1851

function roll_scale_notadj(v)
    -- -- 4922 to 5438
    -- fake to 4800 to 5500
    local l1 = 4800
    local l2 = 5500
    local reso = l2-l1
    local mult = 20000/reso
    local middle = (l2-l1)/2+l1
    return (v-middle)*mult
end

function roll(v)
     val = roll_scale_notadj(v)
     fsim.iocp_write(yoke_roll_ratio_xplane,-val)
end
fsim.chan_evt("19/14","roll")


--- pitch ---
function pitch_scale(v)
    -- 1760 to 3308
    local l1 = 3308
    local l2 = 1760
    local reso = l2-l1
    local mult = 20000/reso
    local middle = (l2-l1)/2+l1

    return (v-middle)*mult
end

function pitch(v)
 fsim.iocp_write(1,-pitch_scale(v))
end
fsim.chan_evt("19/0","pitch")


----- Throttles -----

-- Throttle
-- ch0 1465(idle) - 28 (100%)directo
--  1825 idle rev  2015 max rev

function lscale_th0(v)
    local l1 = 1465
    local l2 = 28
    local reso = l2-l1
    local mult = 10000/reso
    local middle = (l2-l1)/2+l1
    local r = (v-middle)*mult+5000
    return r
end

function scale_th0(v)
   if v > 1500 then throttle_idle = 1 else throttle_idle = 0 end
   if v < 1500 then
      j = lscale_th0(v)
      fsim.iocp_write("3273.0",1)
      fsim.iocp_write("3260.0",j)
      fsim.chan_set("23/11",0)
   else
      local j = (v-1825)*63
      if(j<0) then j=0 end
      fsim.iocp_write("3260.0",j)
      fsim.iocp_write("3273.0",3)
      fsim.chan_set("23/11",3)
   end
end


-- idle 1597
-- full 127
function lscale_th1(v)
    local l1 = 1597
    local l2 = 127
    local reso = l2-l1
    local mult = 10000/reso
    local middle = (l2-l1)/2+l1
    local r = (v-middle)*mult+5000
    return r
end

function scale_th1(v)
   if v > 1500 then throttle_idle = 1 else throttle_idle = 0 end
   if v < 1700 then
      j = lscale_th1(v)
      fsim.iocp_write("3273.1",1)
      fsim.iocp_write("3260.1",j)
      fsim.chan_set("23/7",0)
   else
      local j = (v-1825)*63
      if(j<0) then j=0 end
      fsim.iocp_write("3260.1",j)
      fsim.iocp_write("3273.1",3)
      fsim.chan_set("23/7",3)
   end
end

fsim.chan_evt("24/10","scale_th0")
fsim.chan_evt("24/11","scale_th1")



--- N1 N2
--- N1
n1sim="3275"
n2sim="3276"
tgtsim="3282"

function scalenx(var,ind)
  var=math.floor(var/1000)
  fsim.chan_set(ind,var)
end

function scaletgt(var,ind)
  var=math.floor(var/10000)
  fsim.chan_set(ind,var)
end


function n11simcb(_var)
  var = tonumber(_var)
  scalenx(var,"17/0")
  if var < 305 then
     fsim.chan_set("23/12", 3)
  else
     fsim.chan_set("23/12", 0)
  end
end
function n12simcb(_var)
  var = tonumber(_var)
  scalenx(var,"17/1")
  if var < 305 then
     fsim.chan_set("23/8", 3)
        else
     fsim.chan_set("23/8", 0)
        end
end
function n21simcb(var)
  scalenx(var,"17/2")
end
function n22simcb(var)
  scalenx(var,"17/3")
end

function tgt1simcb(var)
  scaletgt(var,"17/4")
end
function tgt2simcb(var)
  scaletgt(var,"17/5")
end

fsim.iocp_evt(n1sim..".0","n11simcb")
fsim.iocp_evt(n1sim..".1","n12simcb")
fsim.iocp_evt(n2sim..".0","n21simcb")
fsim.iocp_evt(n2sim..".1","n22simcb")
fsim.iocp_evt(tgtsim..".0","tgt1simcb")
fsim.iocp_evt(tgtsim..".1","tgt2simcb")


--- pedals --

function pedals_scale(v)

    local l1 = 1200
    local l2 = 7000
    local reso = l2-l1
    local mult = 20000/reso
    local middle = (l2-l1)/2+l1

    return (v-middle)*mult
    -- -10000 to 10000
end
function wheel_scale(v)
    local l1 = 1200
    local l2 = 7000
    local reso = l2-l1
    local mult = 90/reso
    local middle = (l2-l1)/2+l1

    return (v-middle)*mult
    -- -10000 to 10000
end


function pd(v)
   val = math.floor(pedals_scale(v))
   --print("Out from pedals_scale="..tostring(val))
   --val_wheel = math.floor(wheel_scale(v))
   --print("Out from wheel_scale="..tostring(val_wheel))
   local val = pedals_scale(v)
   fsim.iocp_write("1851", -val)
   --fsim.iocp_write("13", -val_wheel)
   --fsim.iocp_write("13", 0)
end

fsim.chan_evt("26/0", "pd")

----------------------- Parking brk ----
park_brk_button_hw="24/29"
park_brk_led_hw="24/5"

park_brk_xplane=1862
park_brk_zibo=1863
park_brk_led=105

function parking_button(v)
     if v == 1 then fsim.iocp_write(park_brk_xplane,10000) else fsim.iocp_write(park_brk_xplane,0)
   end
     if v == 1 then fsim.iocp_write(park_brk_zibo,10000) else fsim.iocp_write(park_brk_zibo,0)
   end
end

function parking_led(v)
     if v == 0 then fsim.chan_set(park_brk_led_hw,0) else fsim.chan_set(park_brk_led_hw,1) end
end
fsim.chan_evt(park_brk_button_hw,"parking_button")
fsim.iocp_evt(park_brk_led,"parking_led")


--------------


--- Flaps

function val_around(v,limit)
  local tolerance = 50
  if v > (limit-tolerance) and v < (limit+tolerance) then
    return true
  else
    return false
  end
end

function scale_flaps(v)
  local rc = -1

  if val_around(v,1813) then rc = 0 elseif
  val_around(v,1695) then rc = 1250 elseif
  val_around(v,1476) then rc = 2500 elseif
  val_around(v,1380) then rc = 3750 elseif
  val_around(v,1243) then rc = 5000 elseif
  val_around(v,1161) then rc = 6250 elseif
  val_around(v,1058) then rc = 7500 elseif
  val_around(v,910) then rc = 8750 elseif
  val_around(v,738) then rc = 10000 end
  return rc
end

flap_handle_hw = "24/12"
-- flap_dataref=103 - standard
flap_dataref = 7100

function process_flaps(v)
  rc = scale_flaps(v)
  if rc >= 0 then
    fsim.iocp_write(flap_dataref, rc)
  end
end

fsim.chan_evt(flap_handle_hw,"process_flaps")

--------- spoiler -----

spoiler_handle_hw = "24/13"
--spoiler_dataref=104 standard
spoiler_dataref = 7101 -- ixeg

function scale_analog_spoiler(v)
    -- 565 to 1283
    local l1 = 565
    local l2 = 1283
    local reso = l2-l1
    local mult = 10000/reso
    local middle = (l2-l1)/2+l1
    return (v-middle)*mult+5000
end
function scale_spoiler(v)
  if val_around(v,423) then rc = -500 elseif
  val_around(v,565) then rc = 1000 elseif
  val_around(v,1093) then rc = 7400 elseif
  val_around(v,1283) then rc = 10000 else
  rc = scale_analog_spoiler(v) end
  return rc
end

function process_spoiler(v)
  rc = scale_spoiler(v)
  fsim.iocp_write(spoiler_dataref, rc)
end

fsim.chan_evt(spoiler_handle_hw,"process_spoiler")

--[[

Convert angle of trim to yoke position
0 maps to 2500

]]
function ce(deg)
  local l1 = -0.27
  local l2 = 0.27
  -- real hw goes 1700 to 3300
  local x1 = 2000
  local x2 = 3000

  local reso = l2-l1
  local mult = (x2-x1)/reso
  local middle = reso/2+l1
  return ((deg-middle)*mult+(x2-x1)/2)+x1
end

-- force feedback --
-- 1 == hardest
-- 5 = very soft

-- elastic

-- 25=very elastic
-- 20=much
-- 15=little
-- 10=min
function scale_speed_hard(speed)
  -- we want to scale speed (0, 300) to hard (5,1)
  local i1 = 0
  local i2 = 400

  local o1 = 5
  local o2 = 1

  local reso = i2-i1
  local mult = (o2-o1)/reso
  local middle = reso/2+i1
  return math.floor(0.5+((speed-middle)*mult+(o2-o1)/2)+o1)
end
function scale_speed_spring(speed)
  -- we want to scale speed (0, 300) to spring(5,1)
  local i1 = 0
  local i2 = 400

  local o1 = 12
  local o2 = 25

  local reso = i2-i1
  local mult = (o2-o1)/reso
  local middle = reso/2+i1
  return math.floor(0.5+((speed-middle)*mult+(o2-o1)/2)+o1)
end

--
function force_fb(speed)
  --local hard=scale_speed_hard(speed)
  --local spring=scale_speed_spring(speed)
  hard = 13
  sprint = 20
  fsim.chan_set("19/5", spring)
  fsim.chan_set("19/6", hard)
end

--- trim indicator ---
--[[
Elev trim goes from -0.27 to 0.27
Takeoff -0.19 to 0.1

Yoke goes from 1700 to 3300

]]
trim_indicator_hw = "24/8"  -- 0 to 100
trim_indicator_dataref=106 -- sim/flightmodel/controls/elv_trim

function scale_trim(v)
    --[[

    hw:
      60 --> 4 deg
      47  5



    hw: 0 to 80
      -2700 --> 80
      2700  --> 0
    ]]
    local hw_max = 72
    local hw_min = 0
    local scale = 6500/(hw_max - hw_min)
    local v_scale = v/scale   -- -40 to 40
    v_scale = v_scale + (hw_max-hw_min)/2
    v_scale = hw_max-v_scale
    return v_scale
end
function process_trim_indicator(v)
  local scaled_trim = scale_trim(v)
  print("Set scaled trim to " .. scaled_trim)
  fsim.chan_set(trim_indicator_hw,scaled_trim)
  fsim.chan_set("19/4", ce(v/10000))
end

fsim.iocp_evt(trim_indicator_dataref,"process_trim_indicator")

--- Trim control ---
trim_nose_down_hw="24/20"
trim_nose_up_hw="24/21"

fsim.chan_evt(trim_nose_up_hw,"nose_up_cb")
fsim.chan_evt(trim_nose_down_hw,"nose_down_cb")

keep_incr_trim = 0
keep_decr_trim = 0
-- Nose up
-- 20 1
-- 21 0

-- Nose down
-- 20 0
-- 21 0
function nose_up_cb(v)
   if v == 0 and keep_decr_trim == 0 then
     keep_incr_trim = 1
     incr_trim()
   else
    keep_incr_trim = 0

   end
end

function nose_down_cb(v)
         if v == 0 then
                 keep_decr_trim = 1
     decr_trim()
         else
                keep_decr_trim = 0

     end
end


pitch_trim=0
max_pitch_trim = 2700
min_pitch_trim = -2700

function incr_trim()
   fsim.chan_set("24/3",2)
   --print (keep_incr_trim)
   if (pitch_trim < max_pitch_trim) then pitch_trim = pitch_trim + 20 end
   print("incr_trim called, curr="..pitch_trim)
   --fsim.chan_set("19/4", pitch_trim)
   fsim.iocp_write(trim_indicator_dataref,pitch_trim)
   if keep_incr_trim==1 then
      fsim.settimer("incr_trim",50000)
   end
end

function decr_trim()
  fsim.chan_set("24/3",2)
  if (pitch_trim > min_pitch_trim) then pitch_trim = pitch_trim - 20 end
  print("decr_trim called, curr="..pitch_trim)
  fsim.iocp_write(trim_indicator_dataref,pitch_trim)
  if keep_decr_trim==1 then
    fsim.settimer("decr_trim",50000)
  end
end


--- Roll / pitch

function convert_pitch(v)
   local c = v/-1000
   --print("Set pitch to "..c)
   fsim.chan_set("7/4", c)
end

fsim.iocp_evt(3317,"convert_pitch")
fsim.chan_set("7/4",0)

-- Roll
function convert_roll(nsv)

   local v = -nsv/1000
   --print("Roll:"..v)
  --[[
   while v >=360 do
         v = v-360
   end
   while v < 0 do
         v = v+360
   end
   print("Roll2:"..v)
   ]]
   fsim.chan_set("7/0", v)
end

fsim.iocp_evt(3327,"convert_roll")
fsim.chan_set("7/0",0)



---- speed ---
-- speed
speedsim="3301"
speedref="3198"
speeddig="1504"
--[[
3301  sim/cockpit2/gauges/indicators/airspeed_kts_pilot
3198  sim/cockpit2/autopilot/airspeed_dial_kts_mach
1504  sim/flightmodel/misc/machno

]]
function spcb(var)
  local calc_speed = math.floor(var/10000)
  --print("Set speed to "..calc_speed)
  if calc_speed <= 0 then calc_speed = 10 end
  fsim.chan_set("11/2",calc_speed)
  force_fb(calc_speed)

end
fsim.iocp_evt(speedsim,"spcb")
fsim.chan_set("6/2", 1) -- enable alt


function sprefcb(var)
  local calc_speed = math.floor(var/10000)
  fsim.chan_set("11/1",calc_speed)
end
fsim.iocp_evt(speedref,"sprefcb")

function spdig(var)
  fsim.chan_set("11/4", math.floor(var/10))
end
fsim.iocp_evt(speeddig,"spdig")
fsim.chan_set("11/5",1)


-- vsi --
-- 3305  sim/cockpit2/gauges/indicators/vvi_fpm_pilot
vsiiocp=3305
vsi="10/1"

function f_vsi(v)
  fsim.chan_set(vsi,v/10000)
end
fsim.iocp_evt(vsiiocp,"f_vsi")


-- Altimeter
--3303  sim/cockpit2/gauges/indicators/altitude_ft_pilot
function escalt(v)
   if v<0 then v=0 end
   local vesc = v/10000
   --print("Alt="..vesc)
   fsim.chan_set("6/1", vesc)
end
fsim.iocp_evt(3303,"escalt")



-------------- brakes ------

function brake_scale(v)
    -- 8160 (off) to 3700 (on)

    local l1 = 3860
    local l2 = 8100

    mult = 10000/(l2-l1)
    v = v - l1
    v = v * mult
    if (v > 10000) then v=10000 end
    if (v < 0) then v=0 end

    return (10000-v)
end



function brake_left(v)
  fsim.iocp_write("3", brake_scale(v))
end
fsim.chan_evt("26/12", "brake_left")

function brake_right(v)
  fsim.iocp_write("4", brake_scale(v))
end
fsim.chan_evt("26/11", "brake_right")


----


function fsim_connected(state)
  if (state == 0) then
    set_lights(0)
  else
  	set_lights(1)
    set_yoke_defaults()
    set_pedals_defaults()
  end
end


function set_yoke_defaults()
    wait(1)
    fsim.chan_set("19/4", 2300)  -- elev trim
    wait(1)
    fsim.chan_set("19/9", 1) -- motor enable
    fsim.chan_set("19/6", 10)
    fsim.chan_set("19/5", 15)
    wait(1)
end

function set_pedals_defaults()
    fsim.chan_set("26/9", 1) -- motor enable
    fsim.chan_set("26/4", 4000) -- motor enable
end

function set_lights(v)
  if v == 0 then vhi = 0 else vhi = 3 end
  fsim.chan_set(throttle_illum_hw, v)
  fsim.chan_set("7/8", v)         -- Attitude light
  fsim.chan_set("11/5", v)         -- speed light
  fsim.chan_set("10/2", v)         -- vsi light
  fsim.chan_set("6/3", v)         -- vsi light
  fsim.chan_set("19/9", 1) -- motor enable
  fsim.chan_set("20/0", vhi) -- Flaps ind
  fsim.chan_set("20/28", vhi) -- flaps ind

end


fsim.connect_fs('192.168.0.24')
--fsim.connect_fs('192.168.30.160')
